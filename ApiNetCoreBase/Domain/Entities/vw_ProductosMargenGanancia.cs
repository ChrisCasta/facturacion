﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class vw_ProductosMargenGanancia

    {
        public int Prod_ID { get; set; }
        public string Prod_Descripcion { get; set; }
        public decimal MargenDeGanancia { get; set; }
    }
}
