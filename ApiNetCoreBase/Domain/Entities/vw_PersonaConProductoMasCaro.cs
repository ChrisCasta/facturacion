﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class vw_PersonaConProductoMasCaro

    {
        public int Per_ID { get; set; }
        public string Per_Nombre { get; set; }
        public string Per_Apellido { get; set; }
        public string Prod_Descripcion { get; set; }
        public decimal Prod_Precio { get; set; }
    }
}
