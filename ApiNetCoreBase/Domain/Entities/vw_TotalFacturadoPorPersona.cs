﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class vw_TotalFacturadoPorPersona

    {
        public int Per_ID { get; set; }
        public string Per_Nombre { get; set; }
        public string Per_Apellido { get; set; }
        public decimal TotalFacturado { get; set; }
    }
}
