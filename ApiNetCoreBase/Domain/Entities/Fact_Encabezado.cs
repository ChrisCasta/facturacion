﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class Fact_Encabezado

    {
        [Key]
        public int FEnc_ID { get; set; }
        public string FEnc_Numero { get; set; } = string.Empty;
        public DateTime FEnc_Fecha { get; set; }
        public int zPer_ID { get; set; }

        public Persona? Persona { get; set; }
        public ICollection<Fact_Detalle>? Fact_Detalles { get; set; }
    }
}
