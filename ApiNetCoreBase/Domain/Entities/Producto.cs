﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class Producto

    {
        [Key]
        public int Prod_ID { get; set; }
        public string Prod_Descripcion { get; set; } = string.Empty;
        public decimal Prod_Precio { get; set; }
        public decimal Prod_Costo { get; set; }
        public string Prod_UM { get; set; } = string.Empty;
        public ICollection<Fact_Detalle>? Fact_Detalles { get; set; }
    }
}
