﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class vw_ProductosPorUtilidad

    {
        public int Prod_ID { get; set; }
        public string Prod_Descripcion { get; set; }
        public decimal UtilidadGenerada { get; set; }
    }
}
