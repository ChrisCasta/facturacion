﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class Fact_Detalle

    {
        [Key]
        public int FDet_ID { get; set; }
        public int FDet_Linea { get; set; }
        public decimal FDet_Cantidad { get; set; }

        public int zProd_ID { get; set; }
        public decimal FDet_Precio { get; set; }
        public Producto? Producto { get; set; }

        public int zFEnc_ID { get; set; }
        public Fact_Encabezado? Fact_Encabezado { get; set; }
    }
}
