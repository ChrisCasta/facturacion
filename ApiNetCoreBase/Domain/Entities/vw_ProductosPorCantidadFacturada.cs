﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class vw_ProductosPorCantidadFacturada

    {
        public int Prod_ID { get; set; }
        public string Prod_Descripcion { get; set; }
        public decimal CantidadFacturada { get; set; }
    }
}
