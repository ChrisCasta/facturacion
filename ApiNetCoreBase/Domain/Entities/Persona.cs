﻿using System.ComponentModel.DataAnnotations;

namespace Facturacion.Domain.Entities
{
	public class Persona
    {
        [Key]
        public int Per_ID { get; set; }
        public string Per_Nombre { get; set; } =string.Empty;
        public string Per_Apellido { get; set; } = string.Empty;
        public string Per_TipoDocumento { get; set; } = string.Empty;
        public string Per_Documento { get; set; } = string.Empty;
        public ICollection<Fact_Encabezado>? Fact_Encabezados { get; set; }
    }
}
