﻿using Facturacion.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Facturacion.Infrastructure.Repository
{
	public class FacturacionDbContext : DbContext
	{
		public FacturacionDbContext(DbContextOptions<FacturacionDbContext> options) : base(options)
		{

		}

        public DbSet<Persona> Personas { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Fact_Encabezado> Fact_Encabezados { get; set; }
        public DbSet<Fact_Detalle> Fact_Detalles { get; set; }

        public DbSet<vw_TotalFacturadoPorPersona> TotalFacturadoPorPersona { get; set; }
        public DbSet<vw_PersonaConProductoMasCaro> PersonaConProductoMasCaro { get; set; }
        public DbSet<vw_ProductosPorCantidadFacturada> ProductosPorCantidadFacturada { get; set; }
        public DbSet<vw_ProductosPorUtilidad> ProductosPorUtilidad { get; set; }
        public DbSet<vw_ProductosMargenGanancia> ProductosMargenGanancia { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Fact_Encabezado>()
            .HasOne(f => f.Persona)
            .WithMany(p => p.Fact_Encabezados)
            .HasForeignKey(f => f.zPer_ID)
            .OnDelete(DeleteBehavior.Cascade);

            // Configuración de la relación entre Fact_Detalle y Fact_Encabezado
            modelBuilder.Entity<Fact_Detalle>()
                .HasOne(d => d.Fact_Encabezado)
                .WithMany(e => e.Fact_Detalles)
                .HasForeignKey(d => d.zFEnc_ID)
                .OnDelete(DeleteBehavior.Cascade);

            // Configuración de la relación entre Fact_Detalle y Producto
            modelBuilder.Entity<Fact_Detalle>()
                .HasOne(d => d.Producto)
                .WithMany(p => p.Fact_Detalles)
                .HasForeignKey(d => d.zProd_ID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Fact_Detalle>()
            .HasKey(d => d.FDet_ID);

            modelBuilder.Entity<Persona>().ToTable("Persona");
            modelBuilder.Entity<Producto>().ToTable("Producto");
            modelBuilder.Entity<Fact_Encabezado>().ToTable("Fact_Encabezado");
            modelBuilder.Entity<Fact_Detalle>().ToTable("Fact_Detalle");

            modelBuilder.Entity<vw_TotalFacturadoPorPersona>().HasNoKey().ToView("vw_TotalFacturadoPorPersona");
            modelBuilder.Entity<vw_PersonaConProductoMasCaro>().HasNoKey().ToView("vw_PersonaProductoMasCaro");
            modelBuilder.Entity<vw_ProductosPorCantidadFacturada>().HasNoKey().ToView("vw_ProductosPorCantidadFacturada");
            modelBuilder.Entity<vw_ProductosPorUtilidad>().HasNoKey().ToView("vw_ProductosPorUtilidadGenerada");
            modelBuilder.Entity<vw_ProductosMargenGanancia>().HasNoKey().ToView("vw_ProductosConMargenDeGanancia");
        }
    }
}
