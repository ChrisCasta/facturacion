using Facturacion.Infrastructure.Repository;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using System.Text;

var builder = WebApplication.CreateBuilder(args);


try
{
	//logger
	builder.Host.UseSerilog();

	if (Convert.ToBoolean(builder.Configuration["EnableSwagger"].ToString()))
	{

		builder.Services.AddSwaggerGen(c =>
		{
			c.SwaggerDoc("v1", new OpenApiInfo { Title = "Facturación", Version = "v1" });			
			
		});
	}

	builder.Services.AddDbContext<FacturacionDbContext>(
		options =>
			options.UseSqlServer(builder.Configuration.GetConnectionString("Facturacion"))
		);
	
	builder.Services.AddAutoMapper(typeof(Program));
	builder.Services.AddMediatR(typeof(Program));
	builder.Services.AddFluentValidationAutoValidation();
	builder.Services.AddValidatorsFromAssemblyContaining<Program>();
	builder.Services.AddHttpContextAccessor();
	

	builder.Services.AddControllers();

	var app = builder.Build();

	// Configure the HTTP request pipeline.
	if (app.Environment.IsDevelopment())
	{
		app.UseExceptionHandler("/error-development");
	}
	else
	{
		app.UseExceptionHandler("/error");
		app.UseHsts();
	}

	app.UseHttpsRedirection();	

	app.UseAuthentication();
	app.UseAuthorization();	

	app.MapControllers();

	// Enable middleware to serve generated Swagger as a JSON endpoint.
	if (Convert.ToBoolean(builder.Configuration["EnableSwagger"].ToString()))
	{
		app.UseSwagger();
		// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
		// specifying the Swagger JSON endpoint.
		app.UseSwaggerUI(c =>
		{
			c.SwaggerEndpoint("v1/swagger.json", "Facturación");
		});
	}

	app.Run();
}
catch (Exception ex)
{
	Log.Fatal(ex, "Host terminated unexpectedly");
}
finally
{
	Log.CloseAndFlush();
}
