﻿using Facturacion.Application.Features.CQRS.Queries.GetPersonaConProductoMasCaro;
using Facturacion.Application.Features.CQRS.Queries.GetProductosMargenGanancia;
using Facturacion.Application.Features.CQRS.Queries.GetProductosPorCantidadFacturada;
using Facturacion.Application.Features.CQRS.Queries.GetProductosPorUtilidad;
using Facturacion.Application.Features.CQRS.Queries.GetTotalFacturadoPorPersona;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Facturacion.Controllers
{
    [Route("[controller]")]
	[ApiController]
	public class VistasController : ControllerBase
	{
		private ILogger<VistasController> _logger;
		private IMediator _mediator;
        

        public VistasController(IMediator mediator, ILogger<VistasController> logger)
		{
			_logger = logger;
			_mediator = mediator;			
		}

        [HttpGet("TotalFacturadoPorPersona")]
        public async Task<IActionResult> GetTotalFacturadoPorPersona()
        {
            var result = await _mediator.Send(new GetTotalFacturadoPorPersonaRequest());
            return Ok(result.Data);
        }

        [HttpGet("PersonaConProductoMasCaro")]
        public async Task<IActionResult> GetPersonaConProductoMasCaro()
        {
            var result = await _mediator.Send(new GetPersonaConProductoMasCaroRequest());
            return Ok(result.Data);
        }

        [HttpGet("ProductosPorCantidadFacturada")]
        public async Task<IActionResult> GetProductosPorCantidadFacturada()
        {
            var result = await _mediator.Send(new GetProductosPorCantidadFacturadaRequest());
            return Ok(result.Data);
        }

        [HttpGet("ProductosPorUtilidad")]
        public async Task<IActionResult> GetProductosPorUtilidad()
        {
            var result = await _mediator.Send(new GetProductosPorUtilidadRequest());
            return Ok(result.Data);
        }

        [HttpGet("ProductosMargenGanancia")]
        public async Task<IActionResult> GetProductosMargenGanancia()
        {
            var result = await _mediator.Send(new GetProductosMargenGananciaRequest());
            return Ok(result.Data);
        }
    }
}
