﻿using Facturacion.Application.Features.CQRS.Commands.CreateProducto;
using Facturacion.Application.Features.CQRS.Queries.GetProductos;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Facturacion.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class ProductosController : ControllerBase
	{
		private ILogger<ProductosController> _logger;
		private IMediator _mediator;
        

        public ProductosController(IMediator mediator, ILogger<ProductosController> logger)
		{
			_logger = logger;
			_mediator = mediator;			
		}		


		[Route("list")]
		[HttpGet]

		public async Task<IActionResult> GetAllProductos()
		{
			//consulta productos

			var response = await _mediator.Send(new GetProductosRequest());
			if (response != null)
			{
				response.Code = 200;
				response.Message = "Productos encontrados exitosamente";
                return Ok(response);
            }
			else 
			{
                response.Code = 400;
                response.Message = "No se encontraron productos.";
                return BadRequest(response);
			}
			
		
		}

        // POST: api/Producto
        [HttpPost]
        public async Task<IActionResult> PostProducto(CreateProductoCommand command)
        {
            var createProducto = await _mediator.Send(command);
            var getAllProductoResponse = await _mediator.Send(new GetProductosRequest());

            return Ok(new
            {
                ProductoCreado = createProducto,
                Productos = getAllProductoResponse.Data
            });

        }



    }
}
