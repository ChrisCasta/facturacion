﻿using Facturacion.Application.Features.CQRS.Commands.CreatePersona;
using Facturacion.Application.Features.CQRS.Queries.GetPersona;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Facturacion.Controllers
{
    [Route("[controller]")]
	[ApiController]
	public class PersonaController : ControllerBase
	{
		private ILogger<PersonaController> _logger;
		private IMediator _mediator;
        

        public PersonaController(IMediator mediator, ILogger<PersonaController> logger)
		{
			_logger = logger;
			_mediator = mediator;			
		}	

		[Route("list")]
		[HttpGet]

		public async Task<IActionResult> GetAllPersonas()
		{
			//consulta personas

			var response = await _mediator.Send(new GetPersonaRequest());
            if (response != null)
            {
                response.Code = 200;
                response.Message = "Productos encontrados exitosamente";
                return Ok(response);
            }
            else
            {
                response.Code = 400;
                response.Message = "No se encontraron productos.";
                return BadRequest(response);
            }
           
		
		}

        // POST: api/Personas
        [HttpPost]
        public async Task<IActionResult> PostPersona(CreatePersonaCommand command)
        {
            var createPersona = await _mediator.Send(command);
            var getAllPersonasResponse = await _mediator.Send(new GetPersonaRequest());

            return Ok(new
            {
                PersonaCreada = createPersona,
                Personas = getAllPersonasResponse.Data
            });
            
        }
    }
}
