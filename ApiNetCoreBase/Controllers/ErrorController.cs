﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace Facturacion.Controllers
{
	public class ErrorController : ControllerBase
	{
		private ILogger<ErrorController> _logger;
		public ErrorController(ILogger<ErrorController> logger)
		{
			_logger = logger;
		}

		[Route("/error-development")]
		[HttpPost]
		public IActionResult HandleErrorDevelopment([FromServices] IHostEnvironment hostEnvironment)
		{
			if (!hostEnvironment.IsDevelopment())
			{
				return NotFound();
			}

			var exceptionHandlerFeature = HttpContext.Features.Get<IExceptionHandlerFeature>()!;

			var err = Problem(
				detail: exceptionHandlerFeature.Error.StackTrace,
				title: exceptionHandlerFeature.Error.Message);
			_logger.LogError("[ERROR] {@Problem}", err);

			return err;
		}

		[Route("/error")]
		[HttpPost]
		public IActionResult HandleError([FromServices] IHostEnvironment hostEnvironment)
		{

			var exceptionHandlerFeature = HttpContext.Features.Get<IExceptionHandlerFeature>()!;

			var err = Problem(
				detail: exceptionHandlerFeature.Error.StackTrace,
				title: exceptionHandlerFeature.Error.Message);
			_logger.LogError("[ERROR] {@Problem}", err);

			return Problem("Un error inesperado ha ocurrido al procesar la solicitud.");
		}
	}
}
