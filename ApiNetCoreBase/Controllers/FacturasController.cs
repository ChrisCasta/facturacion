﻿using Facturacion.Application.Features.CQRS.Commands.CreateFactura;
using Facturacion.Application.Features.CQRS.Queries.GetFacturas;
using Facturacion.Application.Features.CQRS.Queries.GetPersona;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Facturacion.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class FacturasController : ControllerBase
	{
		private ILogger<FacturasController> _logger;
		private IMediator _mediator;
        

        public FacturasController(IMediator mediator, ILogger<FacturasController> logger)
		{
			_logger = logger;
			_mediator = mediator;			
		}

		[Route("list")]
		[HttpGet]

		public async Task<IActionResult> GetAllFacturas()
		{
			//consulta facturas

			var response = await _mediator.Send(new GetFacturasRequest());
			if(response != null)
				{
				 response.Code = 200;
				 response.Message = "Facturas encontradas con exito."; 
				 return Ok(response);
				}
			else
				{
                 response.Code = 400;
				 response.Message = "No se encuentran facturas registradas.";
				 return BadRequest(response);
				}
			
		
		}

        [HttpPost]
        public async Task<IActionResult> PostFactura([FromBody] CreateFacturaCommand command)
        {
            if (command == null)
            {
                return BadRequest("Datos invalidos.");
            }

            var createFactura = await _mediator.Send(command);
            var getAllFacturasResponse = await _mediator.Send(new GetFacturasRequest());

            return Ok(new
            {
                FacturaCreada = createFactura,
                Facturas = getAllFacturasResponse.Data
            });
        }
    }
}
