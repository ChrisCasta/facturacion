﻿namespace Facturacion.Application.DTO
{
	public class RequestPaginated
	{
		public int PageNumber { get; set; }

		public int PageSize { get; set; }
	}
}
