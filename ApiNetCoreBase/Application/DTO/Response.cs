﻿namespace Facturacion.Application.DTO
{
	public class Response
	{
		public int Code { get; set; }
		public string Message { get; set; } = string.Empty;
	}
}
