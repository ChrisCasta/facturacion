﻿using Facturacion.Application.DTO;
using Facturacion.Domain.Entities;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Queries.GetTotalFacturadoPorPersona
{
    public class GetTotalFacturadoPorPersonaRequest : IRequest<GetTotalFacturadoPorPersonaResponse> 
    {
    }
}
