﻿using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;
using Facturacion.Application.Features.CQRS.Queries.GetProductos;

namespace Facturacion.Application.Features.CQRS.Queries.GetTotalFacturadoPorPersona
{
    public class GetTotalFacturadoPorPersonaQueryHandler : IRequestHandler<GetTotalFacturadoPorPersonaRequest, GetTotalFacturadoPorPersonaResponse>
    {
        private readonly FacturacionDbContext _db;
        private readonly AutoMapper.IConfigurationProvider _configuration;

        public GetTotalFacturadoPorPersonaQueryHandler(FacturacionDbContext db, AutoMapper.IConfigurationProvider configuration)
        {
            _db = db;
            _configuration = configuration;
        }

        public async Task<GetTotalFacturadoPorPersonaResponse> Handle(GetTotalFacturadoPorPersonaRequest request, CancellationToken cancellationToken)
        {
            IQueryable<vw_TotalFacturadoPorPersona> clusterData = _db.TotalFacturadoPorPersona;

            var results = await clusterData.ProjectTo<TotalFacturadoPorPersonaDto>(_configuration).ToListAsync();


            return new GetTotalFacturadoPorPersonaResponse()
            {
                Data = results,
            };
        }
    }
}
