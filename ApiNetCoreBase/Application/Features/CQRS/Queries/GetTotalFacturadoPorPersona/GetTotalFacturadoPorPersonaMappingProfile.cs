﻿using AutoMapper;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetTotalFacturadoPorPersona
{
    public class GetTotalFacturadoPorPersonaMappingProfile : Profile
    {
        public GetTotalFacturadoPorPersonaMappingProfile()
        {
            CreateMap<vw_TotalFacturadoPorPersona, TotalFacturadoPorPersonaDto>();
        }       
    }
}
