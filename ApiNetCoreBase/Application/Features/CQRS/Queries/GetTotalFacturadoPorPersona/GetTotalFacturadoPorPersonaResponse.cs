﻿using Facturacion.Application.DTO;
using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetTotalFacturadoPorPersona
{
	public class GetTotalFacturadoPorPersonaResponse: Response
	{
		public List<TotalFacturadoPorPersonaDto>? Data { get; set; }
	}

    public class TotalFacturadoPorPersonaDto
    {
        public int Per_ID { get; set; }
        public string Per_Nombre { get; set; }
        public string Per_Apellido { get; set; }
        public decimal TotalFacturado { get; set; }
    }
}
