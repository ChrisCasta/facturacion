﻿using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;
using Facturacion.Application.Features.CQRS.Queries.GetTotalFacturadoPorPersona;
using Facturacion.Application.Features.CQRS.Queries.GetProductos;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosMargenGanancia
{
    public class GetProductosMargenGananciaHandler : IRequestHandler<GetProductosMargenGananciaRequest, GetProductosMargenGananciaResponse>
    {
        private readonly FacturacionDbContext _db;
        private readonly AutoMapper.IConfigurationProvider _configuration;

        public GetProductosMargenGananciaHandler(FacturacionDbContext db, AutoMapper.IConfigurationProvider configuration)
        {
            _db = db;
            _configuration = configuration;
        }

        public async Task<GetProductosMargenGananciaResponse> Handle(GetProductosMargenGananciaRequest request, CancellationToken cancellationToken)
        {
            IQueryable<vw_ProductosMargenGanancia> clusterData = _db.ProductosMargenGanancia;

            var results = await clusterData.ProjectTo<ProductosMargenGananciaDto>(_configuration).ToListAsync();


            return new GetProductosMargenGananciaResponse()
            {
                Data = results,
            };
        }
    }
}
