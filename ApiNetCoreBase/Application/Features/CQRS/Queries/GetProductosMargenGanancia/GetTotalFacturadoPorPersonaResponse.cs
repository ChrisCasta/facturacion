﻿using Facturacion.Application.DTO;
using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosMargenGanancia
{
	public class GetProductosMargenGananciaResponse: Response
	{
		public List<ProductosMargenGananciaDto>? Data { get; set; }
	}

    public class ProductosMargenGananciaDto
    {
        public int Prod_ID { get; set; }
        public string Prod_Descripcion { get; set; }
        public decimal MargenDeGanancia { get; set; }
    }
}
