﻿using Facturacion.Application.DTO;
using Facturacion.Domain.Entities;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosMargenGanancia
{
    public class GetProductosMargenGananciaRequest : IRequest<GetProductosMargenGananciaResponse> 
    {
    }
}
