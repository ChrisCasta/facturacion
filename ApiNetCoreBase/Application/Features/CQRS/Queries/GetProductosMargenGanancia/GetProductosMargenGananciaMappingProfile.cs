﻿using AutoMapper;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosMargenGanancia
{
    public class GetProductosMargenGananciaMappingProfile : Profile
    {
        public GetProductosMargenGananciaMappingProfile()
        {
            CreateMap<vw_ProductosMargenGanancia, ProductosMargenGananciaDto>();
        }       
    }
}
