﻿using Facturacion.Application.DTO;
using Facturacion.Domain.Entities;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Queries.GetPersonaConProductoMasCaro
{
    public class GetPersonaConProductoMasCaroRequest : IRequest<GetPersonaConProductoMasCaroResponse> 
    {
    }
}
