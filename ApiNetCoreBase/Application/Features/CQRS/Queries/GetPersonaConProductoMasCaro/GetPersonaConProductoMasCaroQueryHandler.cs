﻿using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;
using Facturacion.Application.Features.CQRS.Queries.GetProductosMargenGanancia;

namespace Facturacion.Application.Features.CQRS.Queries.GetPersonaConProductoMasCaro
{
    public class GetPersonaConProductoMasCaroHandler : IRequestHandler<GetPersonaConProductoMasCaroRequest, GetPersonaConProductoMasCaroResponse>
    {
        private readonly FacturacionDbContext _db;
        private readonly AutoMapper.IConfigurationProvider _configuration;

        public GetPersonaConProductoMasCaroHandler(FacturacionDbContext db, AutoMapper.IConfigurationProvider configuration)
        {
            _db = db;
            _configuration = configuration;
        }

        public async Task<GetPersonaConProductoMasCaroResponse> Handle(GetPersonaConProductoMasCaroRequest request, CancellationToken cancellationToken)
        {
            IQueryable<vw_PersonaConProductoMasCaro> clusterData = _db.PersonaConProductoMasCaro;

            var results = await clusterData.ProjectTo<PersonaConProductoMasCaroDto>(_configuration).ToListAsync();


            return new GetPersonaConProductoMasCaroResponse()
            {
                Data = results,
            };

        }
    }
}
