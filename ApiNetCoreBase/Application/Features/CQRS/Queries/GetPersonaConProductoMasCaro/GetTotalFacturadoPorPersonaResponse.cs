﻿using Facturacion.Application.DTO;
using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetPersonaConProductoMasCaro
{
	public class GetPersonaConProductoMasCaroResponse : Response
	{
		public List<PersonaConProductoMasCaroDto>? Data { get; set; }
	}

    public class PersonaConProductoMasCaroDto
    {
        public int Per_ID { get; set; }
        public string Per_Nombre { get; set; }
        public string Per_Apellido { get; set; }
        public string Prod_Descripcion { get; set; }
        public decimal Prod_Precio { get; set; }
    }
}
