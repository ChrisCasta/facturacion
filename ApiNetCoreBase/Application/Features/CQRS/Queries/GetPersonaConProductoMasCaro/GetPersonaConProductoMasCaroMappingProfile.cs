﻿using AutoMapper;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetPersonaConProductoMasCaro
{
    public class GetPersonaConProductoMasCaroMappingProfile : Profile
    {
        public GetPersonaConProductoMasCaroMappingProfile()
        {
            CreateMap<vw_PersonaConProductoMasCaro, PersonaConProductoMasCaroDto>();
        }       
    }
}
