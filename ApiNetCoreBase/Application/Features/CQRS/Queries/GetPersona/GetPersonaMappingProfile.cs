﻿using Facturacion.Domain.Entities;
using AutoMapper;

namespace Facturacion.Application.Features.CQRS.Queries.GetPersona
{
	public class GetPersonaMappingProfile : Profile
	{
		public GetPersonaMappingProfile()
		{
			CreateMap<Persona,GetPersonaDto>();
		}
	}
}
