﻿using Facturacion.Application.DTO;
using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetPersona
{
	public class GetPersonaResponse:Response
	{
		public List<GetPersonaDto>? Data { get; set; }
	}

	public class GetPersonaDto
	{
        public int Per_ID { get; set; }
        public string Per_Nombre { get; set; } = string.Empty;
        public string Per_Apellido { get; set; } = string.Empty;
        public string Per_TipoDocumento { get; set; } = string.Empty;
        public string Per_Documento { get; set; } = string.Empty;

    }
}
