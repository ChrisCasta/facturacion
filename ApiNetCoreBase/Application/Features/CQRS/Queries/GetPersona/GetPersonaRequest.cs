﻿using Facturacion.Application.DTO;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Queries.GetPersona
{
	public class GetPersonaRequest : RequestPaginated, IRequest<GetPersonaResponse>
	{
		
	}
}
