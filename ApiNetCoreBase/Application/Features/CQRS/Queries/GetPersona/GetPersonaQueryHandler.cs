﻿using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Facturacion.Application.Features.CQRS.Queries.GetPersona
{
    public class GetPersonaQueryHandler : IRequestHandler<GetPersonaRequest, GetPersonaResponse>
    {
		private readonly FacturacionDbContext _db;
		private readonly AutoMapper.IConfigurationProvider _configuration;

		public GetPersonaQueryHandler(FacturacionDbContext db, AutoMapper.IConfigurationProvider configuration)
		{
			_db = db;
			_configuration = configuration;
		}
		public async Task<GetPersonaResponse> Handle(GetPersonaRequest request, CancellationToken cancellation)
		{
			IQueryable<Persona> clusterData = _db.Personas;

			var results = await clusterData.ProjectTo<GetPersonaDto>(_configuration).ToListAsync();
				

			return new GetPersonaResponse()
			{
				Data = results,				
			};
		}
	}
}
