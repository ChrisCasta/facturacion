﻿using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductos
{
    public class GetProductosQueryHandler : IRequestHandler<GetProductosRequest, GetProductosResponse>
    {
		private readonly FacturacionDbContext _db;
		private readonly AutoMapper.IConfigurationProvider _configuration;

		public GetProductosQueryHandler(FacturacionDbContext db, AutoMapper.IConfigurationProvider configuration)
		{
			_db = db;
			_configuration = configuration;
		}
		public async Task<GetProductosResponse> Handle(GetProductosRequest request, CancellationToken cancellation)
		{
			IQueryable<Producto> clusterData = _db.Productos;					

			var results = await clusterData.ProjectTo<GetProductosDto>(_configuration).ToListAsync();
				

			return new GetProductosResponse()
			{
				Data = results,				
			};
		}
	}
}
