﻿using Facturacion.Application.DTO;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductos
{
	public class GetProductosRequest : RequestPaginated, IRequest<GetProductosResponse>
	{
		
	}
}
