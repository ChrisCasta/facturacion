﻿using Facturacion.Domain.Entities;
using AutoMapper;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductos
{
	public class GetProductosMappingProfile : Profile
	{
		public GetProductosMappingProfile()
		{
			CreateMap<Producto,GetProductosDto>();
		}
	}
}
