﻿using Facturacion.Application.DTO;
using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductos
{
	public class GetProductosResponse: Response
	{
		public List<GetProductosDto>? Data { get; set; }
	}

	public class GetProductosDto
	{
        public int Prod_ID { get; set; }
        public string Prod_Descripcion { get; set; } = string.Empty;
        public decimal Prod_Precio { get; set; }
        public decimal Prod_Costo { get; set; }
        public string Prod_UM { get; set; } = string.Empty;
    }
}
