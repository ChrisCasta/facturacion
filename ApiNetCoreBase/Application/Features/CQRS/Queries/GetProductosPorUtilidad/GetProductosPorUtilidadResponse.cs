﻿using Facturacion.Application.DTO;
using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosPorUtilidad
{
	public class GetProductosPorUtilidadResponse : Response
	{
		public List<ProductosPorUtilidadDto>? Data { get; set; }
	}

    public class ProductosPorUtilidadDto
    {
        public int Prod_ID { get; set; }
        public string Prod_Descripcion { get; set; }
        public decimal UtilidadGenerada { get; set; }
    }
}
