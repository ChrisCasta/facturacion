﻿using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;


namespace Facturacion.Application.Features.CQRS.Queries.GetProductosPorUtilidad
{
    public class GetProductosPorUtilidadHandler : IRequestHandler<GetProductosPorUtilidadRequest, GetProductosPorUtilidadResponse>
    {
        private readonly FacturacionDbContext _db;
        private readonly AutoMapper.IConfigurationProvider _configuration;

        public GetProductosPorUtilidadHandler(FacturacionDbContext db, AutoMapper.IConfigurationProvider configuration)
        {
            _db = db;
            _configuration = configuration;

        }

        public async Task<GetProductosPorUtilidadResponse> Handle(GetProductosPorUtilidadRequest request, CancellationToken cancellationToken)
        {
            IQueryable<vw_ProductosPorUtilidad> clusterData = _db.ProductosPorUtilidad;

            var results = await clusterData.ProjectTo<ProductosPorUtilidadDto>(_configuration).ToListAsync();


            return new GetProductosPorUtilidadResponse()
            {
                Data = results,
            };
        }
    }
}
