﻿using Facturacion.Application.DTO;
using Facturacion.Domain.Entities;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosPorUtilidad
{
    public class GetProductosPorUtilidadRequest : IRequest<GetProductosPorUtilidadResponse> 
    {
    }
}
