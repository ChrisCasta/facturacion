﻿using AutoMapper;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosPorUtilidad
{
    public class GetProductosPorUtilidadMappingProfile : Profile
    {
        public GetProductosPorUtilidadMappingProfile()
        {
            CreateMap<vw_ProductosPorUtilidad, ProductosPorUtilidadDto>();
        }       
    }
}
