﻿using AutoMapper;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosPorCantidadFacturada
{
    public class GetProductosPorCantidadFacturadaMappingProfile : Profile
    {
        public GetProductosPorCantidadFacturadaMappingProfile()
        {
            CreateMap<vw_ProductosPorCantidadFacturada, ProductosPorCantidadFacturadaDto>();
        }       
    }
}
