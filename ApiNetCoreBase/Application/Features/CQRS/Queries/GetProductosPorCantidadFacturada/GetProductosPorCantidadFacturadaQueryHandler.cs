﻿using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;


namespace Facturacion.Application.Features.CQRS.Queries.GetProductosPorCantidadFacturada
{
    public class GetProductosPorCantidadFacturadaHandler : IRequestHandler<GetProductosPorCantidadFacturadaRequest, GetProductosPorCantidadFacturadaResponse>
    {
        private readonly FacturacionDbContext _db;
        private readonly AutoMapper.IConfigurationProvider _configuration;

        public GetProductosPorCantidadFacturadaHandler(FacturacionDbContext db, AutoMapper.IConfigurationProvider configuration)
        {
            _db = db;
            _configuration = configuration;
        }

        public async Task<GetProductosPorCantidadFacturadaResponse> Handle(GetProductosPorCantidadFacturadaRequest request, CancellationToken cancellationToken)
        {
            IQueryable<vw_ProductosPorCantidadFacturada> clusterData = _db.ProductosPorCantidadFacturada;

            var results = await clusterData.ProjectTo<ProductosPorCantidadFacturadaDto>(_configuration).ToListAsync();


            return new GetProductosPorCantidadFacturadaResponse()
            {
                Data = results,
            };
        }
    }
}
