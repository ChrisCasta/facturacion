﻿using Facturacion.Application.DTO;
using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosPorCantidadFacturada
{
	public class GetProductosPorCantidadFacturadaResponse : Response
	{
		public List<ProductosPorCantidadFacturadaDto>? Data { get; set; }
	}

    public class ProductosPorCantidadFacturadaDto
    {
        public int Prod_ID { get; set; }
        public string Prod_Descripcion { get; set; }
        public decimal CantidadFacturada { get; set; }
    }
}
