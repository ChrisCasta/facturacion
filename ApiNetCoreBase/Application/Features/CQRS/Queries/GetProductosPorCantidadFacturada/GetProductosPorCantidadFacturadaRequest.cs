﻿using Facturacion.Application.DTO;
using Facturacion.Domain.Entities;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Queries.GetProductosPorCantidadFacturada
{
    public class GetProductosPorCantidadFacturadaRequest : IRequest<GetProductosPorCantidadFacturadaResponse> 
    {
    }
}
