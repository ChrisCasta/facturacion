﻿using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Facturacion.Application.Features.CQRS.Queries.GetFacturas
{
    public class GetFacturasQueryHandler : IRequestHandler<GetFacturasRequest, GetFacturasResponse>
    {
		private readonly FacturacionDbContext _db;
		private readonly AutoMapper.IConfigurationProvider _configuration;

		public GetFacturasQueryHandler(FacturacionDbContext db, AutoMapper.IConfigurationProvider configuration)
		{
			_db = db;
			_configuration = configuration;
		}
		public async Task<GetFacturasResponse> Handle(GetFacturasRequest request, CancellationToken cancellation)
		{
			IQueryable<Fact_Encabezado> clusterData = _db.Fact_Encabezados;

            if (clusterData.Count().Equals(0))
            {
                var result = new List<GetFacturaDto>();

                return new GetFacturasResponse()
                {
                    Data = result,                    
                };

            }
            else
            {
                var query = from encabezado in clusterData
                            join detalle in _db.Fact_Detalles on encabezado.FEnc_ID equals detalle.zFEnc_ID
                            select new GetFacturaDto
                            {
                                EncabezadoID = encabezado.FEnc_ID,
                                Numero = encabezado.FEnc_Numero,
                                Fecha = encabezado.FEnc_Fecha,
                                Cantidad = detalle.FDet_Cantidad,
                                Linea = detalle.FDet_Linea,
                                ProductoID = detalle.zProd_ID
                            };

                var result = query.ToList();

                return new GetFacturasResponse()
                {
                    Data = result,                    
                };
                
            }
		}
	}
}
