﻿using Facturacion.Application.DTO;
using Facturacion.Application.Helpers;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Queries.GetFacturas
{
	public class GetFacturasResponse: Response
	{
		public List<GetFacturaDto>? Data { get; set; }
        
	}

	public class GetFacturaDto
	{       
        public int EncabezadoID { get; set; }
        public string Numero { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Cantidad { get; set; }
        public int Linea { get; set; }
        public int ProductoID { get; set; }       

    }
}
