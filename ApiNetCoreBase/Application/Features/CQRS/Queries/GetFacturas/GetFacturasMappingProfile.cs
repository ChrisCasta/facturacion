﻿using Facturacion.Domain.Entities;
using AutoMapper;

namespace Facturacion.Application.Features.CQRS.Queries.GetFacturas
{
	public class GetFacturasMappingProfile : Profile
	{
		public GetFacturasMappingProfile()
		{
			CreateMap<Fact_Detalle,GetFacturaDto>();
			CreateMap<Fact_Encabezado, GetFacturaDto>();

        }
	}
}
