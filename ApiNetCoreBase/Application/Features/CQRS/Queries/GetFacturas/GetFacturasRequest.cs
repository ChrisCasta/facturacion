﻿using Facturacion.Application.DTO;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Queries.GetFacturas
{
	public class GetFacturasRequest : RequestPaginated, IRequest<GetFacturasResponse>
	{
		
	}
}
