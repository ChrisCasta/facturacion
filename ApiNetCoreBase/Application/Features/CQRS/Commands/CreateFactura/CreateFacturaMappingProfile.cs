﻿using AutoMapper;
using Facturacion.Application.Features.CQRS.Commands.CreateFactura;
using Facturacion.Application.Features.CQRS.Queries.GetFacturas;
using Facturacion.Application.Features.CQRS.Queries.GetProductos;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Commands.CreateProducto
{
    public class CreateFacturaMappingProfile : Profile
    {
        public CreateFacturaMappingProfile()
        {
            CreateMap<Fact_Encabezado, GetFacturaDto>();           
            CreateMap<CreateFacturaDetalleDto, Fact_Detalle>();
        }
    }
}
