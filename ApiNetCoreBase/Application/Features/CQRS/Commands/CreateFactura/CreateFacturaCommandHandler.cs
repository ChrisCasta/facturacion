﻿using AutoMapper;
using Facturacion.Application.Features.CQRS.Queries.GetFacturas;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;

namespace Facturacion.Application.Features.CQRS.Commands.CreateFactura
{
    public class CreateFacturaCommandHandler : IRequestHandler<CreateFacturaCommand, GetFacturaDto>
    {
        private readonly FacturacionDbContext _context;
        private readonly IMapper _mapper;

        public CreateFacturaCommandHandler(FacturacionDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetFacturaDto> Handle(CreateFacturaCommand request, CancellationToken cancellationToken)
        {
            var factura = new Fact_Encabezado
            {
                FEnc_Fecha = request.Fac_Fecha,
                zPer_ID = request.Per_ID,
                Fact_Detalles = request.Detalles.Select(d => new Fact_Detalle
                {
                    zProd_ID = d.Pro_ID,
                    FDet_Cantidad = d.Det_Cantidad,
                    FDet_Precio = d.Det_Precio
                }).ToList()
            };

            _context.Fact_Encabezados.Add(factura);
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<GetFacturaDto>(factura);
        }

    }
}
