﻿using Facturacion.Application.Features.CQRS.Queries.GetFacturas;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Commands.CreateFactura

{
    public class CreateFacturaCommand : IRequest<GetFacturaDto>
    {
        public DateTime Fac_Fecha { get; set; }
        public int Per_ID { get; set; }
        public List<CreateFacturaDetalleDto> Detalles { get; set; }
    }
    public class CreateFacturaDetalleDto
    {
        public int Pro_ID { get; set; }
        public int Det_Cantidad { get; set; }
        public decimal Det_Precio { get; set; }
    }
}
