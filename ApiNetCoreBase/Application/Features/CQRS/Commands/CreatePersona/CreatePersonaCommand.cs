﻿using Facturacion.Application.Features.CQRS.Queries.GetPersona;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Commands.CreatePersona
{
    public class CreatePersonaCommand : IRequest<GetPersonaDto>
    {
        public string Per_Nombre { get; set; }
        public string Per_Apellido { get; set; }
        public string Per_TipoDocumento { get; set; }
        public string Per_Documento { get; set; }
    }
}
