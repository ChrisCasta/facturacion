﻿using AutoMapper;
using Facturacion.Application.Features.CQRS.Queries.GetPersona;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Commands.CreatePersona
{
    public class CreatePersonaMappingProfile : Profile
    {
        public CreatePersonaMappingProfile()
        {
            CreateMap<Persona, GetPersonaDto>();
            CreateMap<CreatePersonaCommand, Persona>();
        }
    }
}
