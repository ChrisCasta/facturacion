﻿using AutoMapper;
using Facturacion.Application.Features.CQRS.Queries.GetPersona;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using MediatR;
using System;

namespace Facturacion.Application.Features.CQRS.Commands.CreatePersona
{
    public class CreatePersonaCommandHandler : IRequestHandler<CreatePersonaCommand, GetPersonaDto>
    {
        private readonly FacturacionDbContext _context;
        private readonly IMapper _mapper;

        public CreatePersonaCommandHandler(FacturacionDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetPersonaDto> Handle(CreatePersonaCommand request, CancellationToken cancellationToken)
        {
            var persona = new Persona
            {
                Per_Nombre = request.Per_Nombre,
                Per_Apellido = request.Per_Apellido,
                Per_TipoDocumento = request.Per_TipoDocumento,
                Per_Documento = request.Per_Documento
            };

            _context.Personas.Add(persona);
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<GetPersonaDto>(persona);
        }

    }
}
