﻿using Facturacion.Application.Features.CQRS.Queries.GetProductos;
using MediatR;

namespace Facturacion.Application.Features.CQRS.Commands.CreateProducto
{
    public class CreateProductoCommand : IRequest<GetProductosDto>
    {
        public string Prod_Descripcion { get; set; }
        public decimal Prod_Precio { get; set; }
        public decimal Prod_Costo { get; set; }
        public string Prod_UM { get; set; }
    }
}
