﻿using AutoMapper;
using Facturacion.Application.Features.CQRS.Queries.GetProductos;
using Facturacion.Domain.Entities;

namespace Facturacion.Application.Features.CQRS.Commands.CreateProducto
{
    public class CreateProductoMappingProfile : Profile
    {
        public CreateProductoMappingProfile()
        {
            CreateMap<Producto, GetProductosDto>();
            CreateMap<CreateProductoCommand, Producto>();
        }
    }
}
