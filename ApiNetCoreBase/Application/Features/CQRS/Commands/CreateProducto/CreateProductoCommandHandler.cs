﻿using AutoMapper;
using Facturacion.Application.Features.CQRS.Queries.GetProductos;
using Facturacion.Domain.Entities;
using Facturacion.Infrastructure.Repository;
using MediatR;
using System;

namespace Facturacion.Application.Features.CQRS.Commands.CreateProducto
{
    public class CreateProductoCommandHandler : IRequestHandler<CreateProductoCommand, GetProductosDto>
    {
        private readonly FacturacionDbContext _context;
        private readonly IMapper _mapper;

        public CreateProductoCommandHandler(FacturacionDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetProductosDto> Handle(CreateProductoCommand request, CancellationToken cancellationToken)
        {
            var producto = new Producto
            {
                Prod_Descripcion = request.Prod_Descripcion,
                Prod_Precio = request.Prod_Precio,
                Prod_Costo = request.Prod_Costo,
                Prod_UM = request.Prod_UM
            };

            _context.Productos.Add(producto);
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<GetProductosDto>(producto);
        }

    }
}
