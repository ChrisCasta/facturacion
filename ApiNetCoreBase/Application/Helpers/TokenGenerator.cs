﻿using System.Security.Cryptography;
using System.Text;

namespace Facturacion.Application.Helpers
{
	public class TokenGenerator
	{
		public static string GetToken(string salt)
		{
			string source = salt + Guid.NewGuid() + DateTime.Now.ToString("yyyy-MM-ddHHmmss");
			using (SHA384 sha384Hash = SHA384.Create())
			{
				//From String to byte array
				byte[] sourceBytes = Encoding.UTF8.GetBytes(source);
				byte[] hashBytes = sha384Hash.ComputeHash(sourceBytes);
				string hash = BitConverter.ToString(hashBytes).Replace("-", string.Empty).ToLower();

				return hash;
			}
		}
	}
}
